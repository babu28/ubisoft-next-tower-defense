#include "stdafx.h"
#include "BaseTower.h"
#include "BaseEnemy.h"


BaseTower::BaseTower(float posX, float posY)
{
	p_positionX = posX;
	p_positionY = posY;
}
void BaseTower::Init()
{
	p_levelUpThreshold = p_currentLevel * 10;
}

void BaseTower::Update(float deltaTime)
{
}

void BaseTower::IncrementXPBy(int exp)
{
}
void BaseTower::Draw()
{
	p_towerSprite->Draw();
	std::string m_scoreText = "LvL:" + std::to_string(p_currentLevel);
	App::Print(p_positionX - 25.0f , p_positionY , m_scoreText.c_str(), 1, 1, 0);
}

void BaseTower::LevelUp()
{
	//---------------------------------------------------------------------------------------------------------------------
	// guard clause to check max level , if max level dont level up
	//---------------------------------------------------------------------------------------------------------------------
	if (p_isMaxLevel == true) { return; }
	
	p_currentLevel++;
	p_towerScale += 0.25f;
	p_towerSprite->SetScale(p_towerScale);
	//---------------------------------------------------------------------------------------------------------------------
	//check if the tower is max level
	//---------------------------------------------------------------------------------------------------------------------
	if(p_currentLevel >= p_maxLevel)
	{
		p_currentLevel = p_maxLevel;
		p_isMaxLevel = true;
		return;
	}
	p_levelUpThreshold = p_currentLevel *  10;

}

void BaseTower::Attack()
{
}

void BaseTower::SetPosition(float x, float y)
{
	p_towerSprite->SetPosition(x, y);
}

BaseTower::~BaseTower()
{
}
