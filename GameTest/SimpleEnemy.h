#pragma once
#include "BaseEnemy.h"
class SimpleEnemy : public BaseEnemy
{

public:
	SimpleEnemy();
	~SimpleEnemy();
	void Init() override;
	void Update(float deltaTime) override;
	void Draw() override;
	void TraversePath() override;

private:
	float m_angle = 90.0f;
	float m_circleSize = 0.0f;
	float m_timer = 0.0f;
	float m_windowCenterX = 0.0f;
	float m_windowCenterY = 0.0f;

	void Reset() override;




};

