#pragma once
class InputController
{
public:
	inline static InputController& Instance()
	{
		static InputController instance;
		return instance;
	}

	void Update(float deltaTime);

private:


	inline explicit InputController()
	{
	}

	inline ~InputController()
	{
	}

	inline explicit InputController(InputController const&)
	{
	}

	inline InputController& operator=(InputController const&)
	{
		return *this;
	}
};

