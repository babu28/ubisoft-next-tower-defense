#pragma once
class EnemySpawner
{
public:
	struct ScaleValues
	{
		float simpleEnemySpawnRate = 0.0f;
		float simpleEnemyNumberIncrease = 0.0f;
		float zombieEnemySpawnRate = 0.0f;
	};

	//---------------------------------------------------------------------------------------------------------------------
	// time is in ms
	//---------------------------------------------------------------------------------------------------------------------
	// #Simple Enemies# Time to wait between each simple enemy spawn
	//---------------------------------------------------------------------------------------------------------------------
	float simepleEnemySpawnRate = 10000.0f;
	//---------------------------------------------------------------------------------------------------------------------
	// #Simple Enemies# spawn time between each simple to create a centipede pattern
	//---------------------------------------------------------------------------------------------------------------------
	float spawmTimeInterval = 500.0f;
	int maxSimpleEnemiesSpawn = 10;
	//---------------------------------------------------------------------------------------------------------------------
	// #Zombie Enemies# Time to wait between each zombie enemy spawn
	//---------------------------------------------------------------------------------------------------------------------
	float zombieEnemySpawnRate = 1000.0f;
	ScaleValues scaleValues;
	float difficultyTimer = 30000.0f;

	
	inline static EnemySpawner& Instance()
	{
		static EnemySpawner instance;
		return instance;
	}
	void Init();
	void Update(float deltaTime);

private:

	float m_gloablTimer = 0.0f;
	float m_simpleEnemytimer = 0.0f;
	float m_zombieEnemytimer = 0.0f;
	float m_intervalTimer = 0.0f;
	float m_timeScaler = 0.0f;
	int m_counter = 0.0f;

	void SimpleEnemySpawn();
	void ZombieEnemySpawn();
	void IncreaseDifficulty();


	inline explicit EnemySpawner()
	{
	}

	inline ~EnemySpawner()
	{
	}

	inline explicit EnemySpawner(EnemySpawner const&)
	{
	}

	inline EnemySpawner& operator=(EnemySpawner const&)
	{
		return *this;
	}
};

