#include "stdafx.h"
#include "InputController.h"
#include "ShockTower.h"
#include "app\app.h"
#include "EnemyManager.h"
#include "TowerManager.h"
#include "TowerPlacement.h"
void InputController::Update(float deltaTime)
{

	if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_UP, true))
	{
		TowerPlacement::Instance().NextTower();
	}

	if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_DOWN, true))
	{
		TowerPlacement::Instance().PreviousTower();
	}

	if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_RIGHT, true))
	{
		TowerPlacement::Instance().NextPosition();
	}

	if (App::GetController().CheckButton(XINPUT_GAMEPAD_DPAD_LEFT, true))
	{
		TowerPlacement::Instance().PreviousPosition();
	}

	if (App::GetController().CheckButton(XINPUT_GAMEPAD_A, true))
	{
		TowerPlacement::Instance().PlaceTower();
	}

}


