#include "stdafx.h"
#include "EnemySpawner.h"
#include "EnemyManager.h"

void EnemySpawner::Init()
{
	scaleValues.simpleEnemyNumberIncrease = 5.0f;
	scaleValues.simpleEnemySpawnRate = 1000.0f;
	scaleValues.zombieEnemySpawnRate = 200.0f;

	m_simpleEnemytimer = simepleEnemySpawnRate;
	m_zombieEnemytimer = spawmTimeInterval;
}

void EnemySpawner::Update(float deltaTime)
{
	m_gloablTimer += deltaTime;
	m_simpleEnemytimer += deltaTime;
	m_zombieEnemytimer += deltaTime;
	m_intervalTimer += deltaTime;

	SimpleEnemySpawn();
	ZombieEnemySpawn();
	if(m_gloablTimer > difficultyTimer)
	{
		IncreaseDifficulty();
	}

}

void EnemySpawner::SimpleEnemySpawn()
{
	if (m_simpleEnemytimer > simepleEnemySpawnRate)
	{
		if (m_intervalTimer > spawmTimeInterval && m_counter <= maxSimpleEnemiesSpawn)
		{
			EnemyManager::Instance().CreateEnemy(1);
			m_counter++;
			m_intervalTimer = 0.0f;
		}
		else if (m_counter > maxSimpleEnemiesSpawn)
		{
			m_simpleEnemytimer = 0.0f;
			m_intervalTimer = 0.0f;
			m_counter = 0;
		}
	}
}

void EnemySpawner::ZombieEnemySpawn()
{

	if (m_zombieEnemytimer > zombieEnemySpawnRate)
	{
		EnemyManager::Instance().CreateEnemy(2);
		m_zombieEnemytimer = 0.0f;
	}
}

void EnemySpawner::IncreaseDifficulty()
{
	maxSimpleEnemiesSpawn += scaleValues.simpleEnemyNumberIncrease;
	simepleEnemySpawnRate -= scaleValues.simpleEnemySpawnRate;
	zombieEnemySpawnRate -= scaleValues.zombieEnemySpawnRate;
	//---------------------------------------------------------------------------------------------------------------------
	//Safty Conditions
	//---------------------------------------------------------------------------------------------------------------------
	//Hopefully the Designers dont trigger these conditions , knowing them they probably will :P
	//---------------------------------------------------------------------------------------------------------------------
	if (simepleEnemySpawnRate <= scaleValues.simpleEnemySpawnRate)
	{
		simepleEnemySpawnRate = scaleValues.simpleEnemySpawnRate;
	}
	if (zombieEnemySpawnRate <= scaleValues.zombieEnemySpawnRate)
	{
		zombieEnemySpawnRate -= scaleValues.zombieEnemySpawnRate;
	}
	m_gloablTimer = 0.0f;
}
