#pragma once

#include <vector>
#include "BaseTower.h"

class TowerPlacement
{

public:
	struct TowerPosition
	{
		float x;
		float y;
	};

	std::vector<TowerPosition> positionList;

	inline static TowerPlacement& Instance()
	{
		static TowerPlacement instance;
		return instance;
	}
	void Render();
	// Expandable for multiple towers
	void PlaceTower(int type = 1);
	void NextPosition();
	void PreviousPosition();
	void NextTower();
	void PreviousTower();

private:
	int m_currentPosition = 0.0f;
	int m_currentTowerType = 1;
	int m_maxTowerTypes = 2;

	//-------------------------------------------------------------------------------------------------------------------
	//the index of this list corresponds to the poisitionList
	//-------------------------------------------------------------------------------------------------------------------
	std::vector<BaseTower*> m_constructedTowersList;

	void RenderTowerBoxes();
	void RenderSelectedTower();
	TowerPlacement();
	inline ~TowerPlacement()
	{
	}

	inline explicit TowerPlacement(TowerPlacement const&)
	{
	}

	inline TowerPlacement& operator=(TowerPlacement const&)
	{
		return *this;
	}

};

