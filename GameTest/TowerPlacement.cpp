#include "stdafx.h"
#include "TowerPlacement.h"
#include "app\app.h"
#include "TowerManager.h"
TowerPlacement::TowerPlacement()
{
	//Created all the possible position that the towers can be placed
	// start at bottom left , clock wise placement
	//-------------------------------------------------------------------------------------------------------------------------------
	//each box is 50 units in legnth , offset 1 unit for rendering 
	//-------------------------------------------------------------------------------------------------------------------------------
	TowerPosition pos;
	// bottom left
	pos.x = 1.0f;
	pos.y = 1.0f;
	positionList.push_back(pos);
	//bottom right
	pos.x = APP_INIT_WINDOW_WIDTH - 51.0f;
	pos.y = 1.0f;
	positionList.push_back(pos);
	//top right
	pos.x = APP_INIT_WINDOW_WIDTH - 51.0f;
	pos.y = APP_INIT_WINDOW_HEIGHT - 51.0f;
	positionList.push_back(pos);
	//top left
	pos.x = 1.0f;
	pos.y = APP_INIT_WINDOW_HEIGHT - 51.0f;
	positionList.push_back(pos);
	//-------------------------------------------------------------------------------------------------------------------------------
	//allocate the empty constructed tower vector
	//the index of constructedTowerList corresponds to the poisitionList
	//-------------------------------------------------------------------------------------------------------------------------------
	for(int i = 0; i < positionList.size(); i++)
	{
		m_constructedTowersList.push_back(nullptr);
	}

}

void TowerPlacement::Render()
{
	RenderTowerBoxes();
	RenderSelectedTower();
}

void TowerPlacement::RenderTowerBoxes()
{
	//Render boxes on possible positions for the towers to be placed
	float r = 1.0f;
	float g = 0.0f;
	float b = 0.0f;
	int listSize = positionList.size();

	for (int i = 0; i < listSize; i++)
	{
		//set color to green current box is selected
		if (i == m_currentPosition)
		{
			r = 0.0f;
			g = 1.0f;
			b = 0.0f;
		}
		else
		{
			r = 1.0f;
			g = 0.0f;
			b = 0.0f;
		}

		App::DrawLine(positionList[i].x, positionList[i].y, positionList[i].x + 50, positionList[i].y, r, g, b);
		App::DrawLine(positionList[i].x + 50, positionList[i].y, positionList[i].x + 50, positionList[i].y + 50, r, g, b);
		App::DrawLine(positionList[i].x + 50, positionList[i].y + 50, positionList[i].x, positionList[i].y + 50, r, g, b);
		App::DrawLine(positionList[i].x, positionList[i].y + 50, positionList[i].x, positionList[i].y, r, g, b);
	}
}

//-------------------------------------------------------------------------------------------------------------------------------
//Indicate the Type of Tower Selected on the UI
//-------------------------------------------------------------------------------------------------------------------------------
void TowerPlacement::RenderSelectedTower()
{
	switch(m_currentTowerType)
	{
		case 1:
		{
			App::Print(100, 700, "Seleced Tower: Shock Tower", 1, 0, 0);
			break;
		}
		case 2:
		{
			App::Print(100, 700, "Seleced Tower: Smite Tower", 0, 1, 1);
			break;
		}
	}
}
void TowerPlacement::PlaceTower(int type)
{
	BaseTower* currentTower = m_constructedTowersList[m_currentPosition];
	if (currentTower != nullptr)
	{
		TowerManager::Instance().RemoveTower(currentTower);
	}
	//send the center point of the current selected tile
	BaseTower* tower = TowerManager::Instance().CreateTower(positionList[m_currentPosition].x + 25.0f,
															positionList[m_currentPosition].y + 25.0f,
															m_currentTowerType);
	//add the tower into the constructed tower list at the current index
	m_constructedTowersList[m_currentPosition] = tower;
}

void TowerPlacement::NextPosition()
{
	m_currentPosition++;
	if (m_currentPosition >= positionList.size()) // not catching the size of the vector as here is a chance for it to change with each level
	{
		m_currentPosition = 0.0f;
	}
}

void TowerPlacement::PreviousPosition()
{
	m_currentPosition--;
	if (m_currentPosition < 0.0f)
	{
		m_currentPosition = positionList.size() - 1.0f; // not catching the size of the vector as here is a chance for it to change with each level
	}
}

//This logic will make sense if these are more than 2 types of towers
void TowerPlacement::NextTower()
{
	m_currentTowerType++;
	if (m_currentTowerType > m_maxTowerTypes)
	{
		m_currentTowerType = 1;
	}
}

//This logic will make sense if these are more than 2 types of towers
void TowerPlacement::PreviousTower()
{
	m_currentTowerType--;
	if (m_currentTowerType < 1)
	{
		m_currentTowerType = m_maxTowerTypes;
	}
}
