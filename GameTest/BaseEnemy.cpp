#include "stdafx.h"
#include "BaseEnemy.h"

BaseEnemy::BaseEnemy()
{
	Init();
}
void BaseEnemy::Init()
{
	p_windowWidth = APP_INIT_WINDOW_WIDTH;
	p_windowHeight = APP_INIT_WINDOW_HEIGHT;
}

void BaseEnemy::Update(float deltaTime)
{
}

void BaseEnemy::TraversePath()
{
}


void BaseEnemy::Draw()
{
	p_enemySprite->Draw();
}

void BaseEnemy::RemoveFromScreen()
{
	p_enemySprite->SetPosition(-100000.0f, -100000.0f);
	isActive = false;
}

void BaseEnemy::Reset()
{
	p_enemySprite->SetPosition(p_windowWidth * 0.5f, p_windowHeight * 0.5f);
	isActive = true;

}

BaseEnemy::~BaseEnemy()
{
}
