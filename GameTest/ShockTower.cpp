#include "stdafx.h"
#include "ShockTower.h"
#include "EnemyManager.h"
#include "ScoreManager.h"

ShockTower::ShockTower(float posX, float posY)
{
	p_positionX = posX;
	p_positionY = posY;
	Init();
}

void ShockTower::Init()
{
	BaseTower::Init();
	p_towerScale = 0.75f;
	//Create the Sprite
	p_towerSprite = App::CreateSprite(".\\TestData\\Ships.bmp", 2, 15);
	p_towerSprite->SetPosition(p_positionX, p_positionY);
	p_towerSprite->SetFrame(14);
	p_towerSprite->SetScale(p_towerScale);
}

void ShockTower::Update(float deltaTime)
{
	//-------------------------------------------------------------------------------------------------------------------
	//attackRate determines the number of attacks per second
	//-------------------------------------------------------------------------------------------------------------------
	m_attackTimer += deltaTime;
	if (m_attackTimer >= attackRate)
	{
		m_isAttacking = true;
		Attack();
		m_attackTimer = 0.0f;
	}
}

void ShockTower::Draw()
{
	BaseTower::Draw();
	if (m_isAttacking == true)
	{
		DrawShockNova();
	}
}

//-------------------------------------------------------------------------------------------------------------------
//Indicator for the Attack range of the tower
//-------------------------------------------------------------------------------------------------------------------
void ShockTower::DrawShockNova()
{
	float towerPosX;
	float towerPosY;
	p_towerSprite->GetPosition(towerPosX, towerPosY);
	float angleA = 0;

	for (int i = 0; angleA <= 360; i++)
	{
		angleA += (2 * 3.14159f) / 360;
		//-------------------------------------------------------------------------------------------------------------------
		//store the width and height of the window insted of calling the macro
		//change shockRadius to vary the size of the circle
		//-------------------------------------------------------------------------------------------------------------------
		float xPos = sinf(angleA) * attackRadius + towerPosX;
		float yPos = cosf(angleA) * attackRadius + towerPosY;
		App::DrawLine(xPos, yPos, xPos + 5, yPos + 5, 1, 1, 0);
		m_isAttacking = false;
	}
}

void ShockTower::LevelUp()
{
	BaseTower::LevelUp();
	attackRadius += m_attackRadiusLevelUpValue;
	attackRate -= m_attackRateLevelUpValue;
}

void ShockTower::IncrementXPBy(int exp)
{
	//-------------------------------------------------------------------------------------------------------------------
	// guard clause to check max level , if max level dont increase XP
	//-------------------------------------------------------------------------------------------------------------------
	if (p_isMaxLevel == true) { return; }

	p_experiencePoints += exp;
	if (p_experiencePoints >= p_levelUpThreshold)
	{
		LevelUp();
	}
}

void ShockTower::Attack()
{
	float collisionX, collisionY, deltaX, deltaY, distanceSquared;
	//-------------------------------------------------------------------------------------------------------------------
	// giving all object a radius of 60 , ideally each object would return its own radius
	//-------------------------------------------------------------------------------------------------------------------
	float radiusSquared = attackRadius * attackRadius + 60 * 60;
	std::vector<BaseEnemy*> allEnemies = EnemyManager::Instance().GetAllEnemies();
	//-------------------------------------------------------------------------------------------------------------------
	//Check for collision with each enemy when the tower attacks (NOT called every frame , dependent of the attack rate)
	//TODO:: find a better way to do colliisons
	//-------------------------------------------------------------------------------------------------------------------
	for each (BaseEnemy * collisionEnemy in allEnemies)
	{
		collisionEnemy->p_enemySprite->GetPosition(collisionX, collisionY);
		deltaX = p_positionX - collisionX;
		deltaY = p_positionY - collisionY;
		distanceSquared = (deltaX * deltaX) + (deltaY * deltaY);
		//-------------------------------------------------------------------------------------------------------------------
		//can also add the radius of the coliding object to get a better collision
		//-------------------------------------------------------------------------------------------------------------------
		//more optimized to check the squared of the distance insted of calculating the square root.
		//-------------------------------------------------------------------------------------------------------------------

		if (distanceSquared < radiusSquared)
		{
			IncrementXPBy(1);
			//---------------------------------------------------------------------------------------------------------------------
			//increament the score 
			//---------------------------------------------------------------------------------------------------------------------
			ScoreManager::Instance().IncrementScoreBy(1);
			EnemyManager::Instance().RemoveEnemy(collisionEnemy);
		}
	}
}

void ShockTower::SetPosition(float x, float y)
{
	BaseTower::SetPosition(x, y);
}