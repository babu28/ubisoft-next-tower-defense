#pragma once
#include "BaseTower.h"
class SmiteTower :public BaseTower
{
public:
	//-------------------------------------------------------------------------------------------------------------------
	// number of the enemies the tower can attack at random at once
	//-------------------------------------------------------------------------------------------------------------------
	float maxAttackCount = 1.0f;
	//-------------------------------------------------------------------------------------------------------------------
	// attack per second , time is in ms
	//-------------------------------------------------------------------------------------------------------------------
	float attackRate = 5000.0f;

	SmiteTower(float posX = 0.0f, float posY = 0.0f);
	~SmiteTower() {};
	void Init() override;
	void Update(float deltaTime) override;
	void Draw() override;
	void SetPosition(float x, float y) override;

private:
	bool m_isAttacking = false;
	float m_attackTimer = 0.0f;
	int m_attackCount = 0;
	float m_attackRateLevelUpValue = 1000.0f;
	float m_enemyAttackCountIncrease = 1.0f;
	float enemyPosX = 0.0f;
	float enemyPosY = 0.0f;

	void LevelUp() override;
	void IncrementXPBy(int exp) override;
	void Attack() override;
};

