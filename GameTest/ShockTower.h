#pragma once
#include "BaseTower.h"
class ShockTower : public BaseTower
{
public:
	//-------------------------------------------------------------------------------------------------------------------
	// radius of the damaging circle around the tower
	//-------------------------------------------------------------------------------------------------------------------
	float attackRadius = 350.0f;
	//-------------------------------------------------------------------------------------------------------------------
	// attack per second , time is in ms
	//-------------------------------------------------------------------------------------------------------------------
	float attackRate = 3000.0f;

	ShockTower(float posX = 0.0f, float posY = 0.0f);
	~ShockTower(){};
	void Init() override;
	void Update(float deltaTime) override;
	void Draw() override;
	void SetPosition(float x, float y) override;


private:
	
	bool m_isAttacking = false;
	float m_attackTimer = 0.0f;
	float m_attackRateLevelUpValue = 1000.0f;
	float m_attackRadiusLevelUpValue = 30.0f;

	void LevelUp() override;
	void IncrementXPBy(int exp) override;
	void Attack() override;
	void DrawShockNova();

};

