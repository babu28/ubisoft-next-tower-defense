#include "stdafx.h"
#include "SimpleEnemy.h"

SimpleEnemy::SimpleEnemy()
{
	Init();
}


void SimpleEnemy::Init()
{
	BaseEnemy::Init();

	//Get the Center of the Window
	m_windowCenterX = p_windowWidth * 0.5f;
	m_windowCenterY = p_windowHeight * 0.5f;

	//Create the Sprite
	p_enemySprite = App::CreateSprite(".\\TestData\\Ships.bmp", 2, 12);
	p_enemySprite->SetPosition(m_windowCenterX, m_windowCenterY);
	p_enemySprite->SetFrame(2);
	p_enemySprite->SetScale(0.75f);

	//-----------------------------------------------------------------------------------------------------------------------
	//Type of enemy this object is
	//-----------------------------------------------------------------------------------------------------------------------
	type = 1;
}

void SimpleEnemy::Update(float deltaTime)
{
	TraversePath();
}

void SimpleEnemy::TraversePath()
{
	//---------------------------------------------------------------------------------------------------------------------
	//Logic for Spiral Path
	//---------------------------------------------------------------------------------------------------------------------

	m_angle += 2 * 3.14159 / 360;
	m_circleSize += 0.15f;

	float xPos = sinf(m_angle * p_speed) * m_circleSize;
	float yPos = cosf(m_angle * p_speed) * m_circleSize;
	p_enemySprite->SetPosition(xPos + m_windowCenterX , yPos + m_windowCenterY);
}

void SimpleEnemy::Reset()
{
	m_circleSize = 0.0f;
	m_angle = 0.0f;
	BaseEnemy::Reset();
}

void SimpleEnemy::Draw()
{
	BaseEnemy::Draw();
}

SimpleEnemy::~SimpleEnemy()
{

}

