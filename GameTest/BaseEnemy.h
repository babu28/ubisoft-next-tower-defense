#pragma once
#include "app\app.h"

class BaseEnemy
{
public:
	CSimpleSprite* p_enemySprite = nullptr;
	bool isActive = true;
	int type = 0;
	
	BaseEnemy();
	virtual ~BaseEnemy();
	virtual void Update(float deltaTime);
	virtual void Draw();
	virtual void RemoveFromScreen();
	virtual void Reset();

protected:

	float p_windowWidth = 0.0f;
	float p_windowHeight = 0.0f;
	float p_speed = 0.5f;

	virtual void Init();
	virtual void TraversePath();

};
