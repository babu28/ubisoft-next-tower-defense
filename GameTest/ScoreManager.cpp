#include "stdafx.h"
#include "ScoreManager.h"
#include "app\app.h"

void ScoreManager::Init()
{
	m_gametimer = maxGameTime;
}

void ScoreManager::Update(float deltaTime)
{
	if (m_isGameOver == false)
	{
		m_gametimer -= deltaTime;
		if (m_gametimer <= 0)
		{
			m_isGameOver = true;
		}
	}
}

void ScoreManager::Render()
{
	//float to string conversion
	std::string m_scoreText = "Score: " + std::to_string(m_score);
	int timeInInt = (int)(m_gametimer * 0.001f);
	std::string m_timerText = "Time: " + std::to_string(timeInInt) + " sec";;

	App::Print(480, 700, m_scoreText.c_str());
	App::Print(680, 700, m_timerText.c_str() , 0 , 1 , 0);
}

void ScoreManager::IncrementScoreBy(int score)
{
	m_score += score;
}

int ScoreManager::GetScore()
{
	return m_score;
}

bool ScoreManager::IsGameOver()
{
	return m_isGameOver;
}