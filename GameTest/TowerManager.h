#pragma once
#include <vector>
#include "BaseTower.h"
#include <list>

class TowerManager
{
public:
	inline static TowerManager& Instance()
	{
		static TowerManager instance;
		return instance;
	}

	void RenderTowers();
	void Update(float deltaTime);
	
	//Expandable for more towers
	BaseTower* CreateTower(float posX = 0.0f,float posY = 0.0f ,int type = 1);
	void RemoveTower(BaseTower* tower);
	~TowerManager();


private:
	std::list<BaseTower*> m_towerList;

private:

	inline explicit TowerManager()
	{
	}


	inline explicit TowerManager(TowerManager const&)
	{
	}

	inline TowerManager& operator=(TowerManager const&)
	{
		return *this;
	}
};

