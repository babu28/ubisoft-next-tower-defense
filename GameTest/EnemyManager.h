#pragma once
#include"BaseEnemy.h"
#include <list>
class EnemyManager
{
public:

	int poolSize = 400;


	inline static EnemyManager& Instance()
	{
		static EnemyManager instance;
		return instance;
	}
	void RenderEnemies();
	void Update(float deltaTime);
	void Init();
	//-------------------------------------------------------------------------------------------------------------------
	//Expandable for more enemies by passing different enemy type
	//-------------------------------------------------------------------------------------------------------------------
	void CreateEnemy(int enemyType = 1);
	void RemoveEnemy(BaseEnemy* enemy);
	std::vector<BaseEnemy*> GetAllEnemies();
	~EnemyManager();

private:
	//-------------------------------------------------------------------------------------------------------------------
	//colliision Data
	//-------------------------------------------------------------------------------------------------------------------
	float m_collisionBorderSize = 75.0f;
	float m_borderWidth = APP_INIT_WINDOW_WIDTH - m_collisionBorderSize;
	float m_borderHeight = APP_INIT_WINDOW_HEIGHT - m_collisionBorderSize;
	//-------------------------------------------------------------------------------------------------------------------

	std::vector<BaseEnemy*> m_enemieList;

	void CreateNewInEnemyInPool(int type);
	bool KillIfOutOfBorder(BaseEnemy* enemy);
	inline explicit EnemyManager()
	{
	}

	inline explicit EnemyManager(EnemyManager const&)
	{
	}

	inline EnemyManager& operator=(EnemyManager const&)
	{
		return *this;
	}
};

