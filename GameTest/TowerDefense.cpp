#include "stdafx.h"
#include <windows.h>
#include <math.h>
#include "app\app.h"
#include "InputController.h"
#include "TowerManager.h"
#include "EnemyManager.h"
#include "TowerPlacement.h"
#include "ScoreManager.h"
#include "EnemySpawner.h"

float windowWidth = 0.0f;
float windowHeight = 0.0f;
float screenCenterX = 0.0f;
float screenCenterY = 0.0f;
//---------------------------------------------------------------------------------------------------------------------
//Size of the outer Border
//---------------------------------------------------------------------------------------------------------------------
float borderSize = 50.0f;
bool pauseGame = true;
bool isGameOver = false;
void DisplayBorder();
void PauseMenuDisplay();
void GameOverScreen();

void Init()
{
	windowWidth = APP_INIT_WINDOW_WIDTH;
	windowHeight = APP_INIT_WINDOW_HEIGHT;
	screenCenterX = windowWidth * 0.5f;
	screenCenterY = windowHeight * 0.5f;

	ScoreManager::Instance().Init();
	EnemyManager::Instance().Init();
	EnemySpawner::Instance().Init();
}
void Update(float deltaTime)
{
	//---------------------------------------------------------------------------------------------------------------------
	//Guard Clause to check for game over
	//---------------------------------------------------------------------------------------------------------------------
	if (isGameOver == true) { return; }

	isGameOver = ScoreManager::Instance().IsGameOver();
	if (App::GetController().CheckButton(VK_F1,true))
	{
		//toggle boolean when 1 is pressed on the keybard
		pauseGame = 1 - pauseGame;
	}
	if (pauseGame == false)
	{
		InputController::Instance().Update(deltaTime);
		ScoreManager::Instance().Update(deltaTime);
		EnemyManager::Instance().Update(deltaTime);
		TowerManager::Instance().Update(deltaTime);
		EnemySpawner::Instance().Update(deltaTime);
	}
}

void Render()
{
	//---------------------------------------------------------------------------------------------------------------------
	//Guard Clause to check for game over
	//---------------------------------------------------------------------------------------------------------------------
	if (isGameOver == true)
	{ 
		GameOverScreen();
		return;
	}

	if (pauseGame == false)
	{
		ScoreManager::Instance().Render();
		EnemyManager::Instance().RenderEnemies();
		TowerManager::Instance().RenderTowers();
		TowerPlacement::Instance().Render();
		DisplayBorder();
	}
	else
	{
		PauseMenuDisplay();
	}
}
//---------------------------------------------------------------------------------------------------------------------
//Creates the outer Border for level
//---------------------------------------------------------------------------------------------------------------------

void DisplayBorder()
{
	App::DrawLine(borderSize, borderSize, windowWidth - borderSize, borderSize, 0, 0, 1);
	App::DrawLine(windowWidth - borderSize, borderSize, windowWidth - borderSize, windowHeight - borderSize, 0, 0, 1);
	App::DrawLine(windowWidth - borderSize, windowHeight - borderSize, borderSize, windowHeight - borderSize, 0, 0, 1);
	App::DrawLine(borderSize, windowHeight - borderSize, borderSize, borderSize, 0, 0, 1);
}
//---------------------------------------------------------------------------------------------------------------------
// Pause Screen
//---------------------------------------------------------------------------------------------------------------------
void PauseMenuDisplay()
{
	App::Print((screenCenterX) - 150.0f, (screenCenterY) + 50.0f, "GAME PAUSE!. Press 1 to Unpause", 1, 0, 0.5);
	App::Print((screenCenterX) - 50.0f,	  screenCenterY, "Controlls", 0, 1, 0);
	App::Print((screenCenterX) - 200.0f, (screenCenterY) - 40.0f, "<-- Left Arrow and Right Arrow --> to switch place", 1, 1, 1);
	App::Print((screenCenterX) - 200.0f, (screenCenterY) - 80.0f, " Up Arrow and Down Arrow  to Change type of Tower", 1, 1, 1);
	App::Print((screenCenterX) - 130.0f, (screenCenterY) - 120.0f, " Spacebar to construct a Tower", 1, 1, 1);
}
//---------------------------------------------------------------------------------------------------------------------
//Game over screen
//---------------------------------------------------------------------------------------------------------------------
void GameOverScreen()
{
	int timeInInt = ScoreManager::Instance().GetScore();
	std::string scoreText = "Score: " + std::to_string(timeInInt);;
	App::Print((screenCenterX) - 60.0f, (screenCenterY) + 25.0f, "GAME OVER!", 1, 0, 0);
	App::Print((screenCenterX) - 40.0f, (screenCenterY) - 25.0f, scoreText.c_str(), 0, 1, 0);
	App::Print((screenCenterX) - 50.0f, (screenCenterY) -50.0f, "ESC to Quit", 0, 0, 1);
}

void Shutdown()
{
	//---------------------------------------------------------------------------------------------------------------------
	//memory clean up will happen when the managers destructors are called for the objects in the scene
	//---------------------------------------------------------------------------------------------------------------------
}