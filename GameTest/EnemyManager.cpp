#include "stdafx.h"
#include "EnemyManager.h"
#include "SimpleEnemy.h"
#include "ZombieEnemy.h"

//---------------------------------------------------------------------------------------------------------------------
//Create enemies at the start for the object Pool
//---------------------------------------------------------------------------------------------------------------------
void EnemyManager::Init()
{
	BaseEnemy* enemy = nullptr;
	for (int i = 0; i < poolSize; i++)
	{
		if (i < poolSize * 0.5f)
		{
			enemy = new SimpleEnemy();
			m_enemieList.push_back(enemy);
		}
		else
		{
			enemy = new ZombieEnemy();
			m_enemieList.push_back(enemy);
		}
		enemy->RemoveFromScreen();
		enemy->isActive = false;
	}
}

void EnemyManager::RenderEnemies()
{
	//call draw for every enemy
	for each (BaseEnemy* enemy in m_enemieList)
	{
		if (enemy->isActive == true)
		{
			enemy->Draw();
		}
	}
}

void EnemyManager::Update(float deltaTime)
{
	//call update for every enemy
	for each (BaseEnemy* enemy in m_enemieList)
	{
		if (enemy->isActive == true)
		{
			enemy->Update(deltaTime);
			bool isOutOfBorder = KillIfOutOfBorder(enemy);
			if (isOutOfBorder == true) { break; }
		}
	}
}


//---------------------------------------------------------------------------------------------------------------------
//Destroy if the enemy is out the level(blue) border
//---------------------------------------------------------------------------------------------------------------------
bool EnemyManager::KillIfOutOfBorder(BaseEnemy* enemy)
{
	float x, y;
	enemy->p_enemySprite->GetPosition(x, y);
	if (x < 80 || x > m_borderWidth || y < 80 || y > m_borderHeight)
	{
		RemoveEnemy(enemy);
		return true;
	}
	return false;
}

void EnemyManager::CreateEnemy(int enemyType)
{
	int maxSize = m_enemieList.size();
	for (int i = 0; i < maxSize; i++)
	{
		if (m_enemieList[i]->type == enemyType && m_enemieList[i]->isActive == false)
		{
			m_enemieList[i]->Reset();
			return;
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	// if it reaches this point then these is no object avaliable in the pool , Create a new object
	//---------------------------------------------------------------------------------------------------------------------
	CreateNewInEnemyInPool(enemyType);
}

void EnemyManager::CreateNewInEnemyInPool(int type)
{
	BaseEnemy* enemy = nullptr;
	switch (type)
	{
	case 1:
	{
		enemy = new SimpleEnemy();
		break;
	}
	case 2:
	{
		enemy = new ZombieEnemy();
		break;
	}

	}
	m_enemieList.push_back(enemy);
}


std::vector<BaseEnemy*> EnemyManager::GetAllEnemies()
{
	return m_enemieList;
}

void EnemyManager::RemoveEnemy(BaseEnemy* enemy)
{
	//---------------------------------------------------------------------------------------------------------------------
	// Play Destroy Sound
	//---------------------------------------------------------------------------------------------------------------------
	//App::PlaySound(".\\TestData\\test.wav");
	enemy->RemoveFromScreen();
	enemy->isActive = false;
}

EnemyManager::~EnemyManager()
{
	for each (BaseEnemy* enemy in m_enemieList)
	{
		delete enemy;
		enemy = nullptr;
	}
}