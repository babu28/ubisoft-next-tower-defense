#include "stdafx.h"
#include "ZombieEnemy.h"

ZombieEnemy::ZombieEnemy()
{
	Init();
}

void ZombieEnemy::Init()
{
	//Get the Center of the Window
	m_spawnPosX = p_windowWidth * 0.5f;
	m_spawnPosY = p_windowHeight * 0.5f;

	//Create the Sprite
	p_enemySprite = App::CreateSprite(".\\TestData\\Ships.bmp", 2, 40);
	p_enemySprite->SetPosition(m_spawnPosX, m_spawnPosY);
	p_enemySprite->SetFrame(2);
	p_enemySprite->SetScale(0.75f);
	//-----------------------------------------------------------------------------------------------------------------------
	//Type of enemy this object is
	//-----------------------------------------------------------------------------------------------------------------------
	type = 2;

	SetRandomDirection();

}

void ZombieEnemy::Update(float deltaTime)
{
	TraversePath();
}

void ZombieEnemy::Draw()
{
	BaseEnemy::Draw();
}

void ZombieEnemy::TraversePath()
{
	m_spawnPosX += m_moveToPosX * p_speed;
	m_spawnPosY += m_moveToPosY * p_speed;
	p_enemySprite->SetPosition(m_spawnPosX, m_spawnPosY);
}

void ZombieEnemy::SetRandomDirection()
{
	//-----------------------------------------------------------------------------------------------------------------------
	//Pick a Random Direction to move in	
	//-----------------------------------------------------------------------------------------------------------------------
	m_moveToPosX = cosf(FRAND_RANGE(-1, 1));
	m_moveToPosY = sinf(FRAND_RANGE(-1, 1));
	//-----------------------------------------------------------------------------------------------------------------------
	//TODO:: Try to do it with Macro
	// pick to go left or right
	//-----------------------------------------------------------------------------------------------------------------------
	int rand = (std::rand() > RAND_MAX / 2) ? -1 : 1;
	p_speed = 0.5f * rand;
}

void ZombieEnemy::Reset()
{
	m_spawnPosX = p_windowWidth * 0.5f;
	m_spawnPosY = p_windowHeight * 0.5f;
	SetRandomDirection();
	BaseEnemy::Reset();
}

ZombieEnemy::~ZombieEnemy()
{
}
