#pragma once
#include "BaseEnemy.h"
class ZombieEnemy : public BaseEnemy
{
public:
	ZombieEnemy();
	~ZombieEnemy();
	void Init() override;
	void Update(float deltaTime) override;
	void Draw() override;
	void TraversePath() override;

private:
	float m_spawnPosX = 0.0f;
	float m_spawnPosY = 0.0f;
	float m_moveToPosX = 0.0f;
	float m_moveToPosY = 0.0f;


	void Reset() override;
	void SetRandomDirection();
};

