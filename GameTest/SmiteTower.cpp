#include "stdafx.h"
#include <list>
#include "SmiteTower.h"
#include "BaseEnemy.h"
#include "EnemyManager.h"
#include "ScoreManager.h"

SmiteTower::SmiteTower(float posX, float posY)
{
	p_positionX = posX;
	p_positionY = posY;
	Init();
}

void SmiteTower::Init()
{
	BaseTower::Init();
	//Create the Sprite
	p_towerScale = 0.5f;
	p_towerSprite = App::CreateSprite(".\\TestData\\Ships.bmp", 2, 13);
	p_towerSprite->SetPosition(p_positionX, p_positionY);
	p_towerSprite->SetFrame(6);
	p_towerSprite->SetScale(p_towerScale);
}

void SmiteTower::Update(float deltaTime)
{
	//-------------------------------------------------------------------------------------------------------------------
	//attackRate determines the number of attacks per second
	//-------------------------------------------------------------------------------------------------------------------
	//logic is being reset in render
	//-------------------------------------------------------------------------------------------------------------------
	m_attackTimer += deltaTime;
	if (m_attackTimer >= attackRate)
	{
		Attack();
		m_isAttacking = true;
	}
}

void SmiteTower::Draw()
{
	BaseTower::Draw();
	if (m_isAttacking == true)
	{
		App::DrawLine(p_positionX, p_positionY, enemyPosX, enemyPosY, 0.3f, 1.0f, 1.0f);
		//-------------------------------------------------------------------------------------------------------------------
		//Set the logic of stop attacking in render be get the last line draw
		//TODO:: find a better way to represent attack Incication for this tower
		//-------------------------------------------------------------------------------------------------------------------
		if (m_attackCount >= maxAttackCount)
		{
			m_isAttacking = false;
			m_attackTimer = 0.0f;
			m_attackCount = 0.0f;
		}
	}
}

void SmiteTower::SetPosition(float x, float y)
{
	BaseTower::SetPosition(x, y);
}

void SmiteTower::LevelUp()
{
	BaseTower::LevelUp();
	maxAttackCount += m_enemyAttackCountIncrease;
	attackRate -= m_attackRateLevelUpValue;
}

void SmiteTower::IncrementXPBy(int exp)
{
	//-------------------------------------------------------------------------------------------------------------------
	// guard clause to check max level , if max level dont increase XP
	//-------------------------------------------------------------------------------------------------------------------
	if (p_isMaxLevel == true) { return; }

	p_experiencePoints += exp;
	if (p_experiencePoints >= p_levelUpThreshold)
	{
		LevelUp();
	}
}

//-------------------------------------------------------------------------------------------------------------------
//Calling attack each frame insted of destroying at once in a loop, seems more realestic
//-------------------------------------------------------------------------------------------------------------------
void SmiteTower::Attack()
{
	std::vector<BaseEnemy*> allEnemies = EnemyManager::Instance().GetAllEnemies();
	//-------------------------------------------------------------------------------------------------------------------
	//dont attack if there are no enemies, duh!!
	//-------------------------------------------------------------------------------------------------------------------
	if (allEnemies.size() == 0) { return; }
	//-------------------------------------------------------------------------------------------------------------------
	//Select a Random enemy index to Cull withing the list maxsize
	//-------------------------------------------------------------------------------------------------------------------
	int randVal = FRAND_RANGE(1, allEnemies.size());
	if (allEnemies[randVal] != nullptr && allEnemies[randVal]->isActive == true)
	{
		allEnemies[randVal]->p_enemySprite->GetPosition(enemyPosX, enemyPosY);
		EnemyManager::Instance().RemoveEnemy(allEnemies[randVal]);
		IncrementXPBy(1);
		ScoreManager::Instance().IncrementScoreBy(1);
		m_attackCount++;
	}
}