#include "stdafx.h"
#include "TowerManager.h"
#include "ShockTower.h"
#include "SmiteTower.h"
//---------------------------------------------------------------------------------------------------------------------
//TODO:: replace std::list and change create and remove functunoins to support OjectPooling
//---------------------------------------------------------------------------------------------------------------------
void TowerManager::RenderTowers()
{
	//call draw on every tower
	for each (BaseTower* tower in m_towerList)
	{
		tower->Draw();
	}
}

void TowerManager::Update(float deltaTime)
{
	//call update on every tower
	for each (BaseTower* tower in m_towerList)
	{
		tower->Update(deltaTime);
	}
}

BaseTower* TowerManager::CreateTower(float posX, float posY, int type)
{
	BaseTower* tower;
	//pass the position of the selected tile for placing the tower
	switch (type)
	{
		case 1:
		{
			tower = new ShockTower(posX, posY);
			break;
		}
		case 2:
		{
			tower = new SmiteTower(posX, posY);
			break;
		}
	}
	m_towerList.push_back(tower);
	return tower;
}

void TowerManager::RemoveTower(BaseTower* tower)
{
	for each (BaseTower* t in m_towerList)
	{
		if (t == tower)
		{
			m_towerList.remove(t);
			delete t;
			t = nullptr;
			break;
		}
	}
}

TowerManager::~TowerManager()
{
	for each (BaseTower* tower in m_towerList)
	{
		delete tower;
		tower = nullptr;
	}
}