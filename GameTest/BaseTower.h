#pragma once
#include "app\app.h"

class BaseTower
{
public:
	CSimpleSprite* p_towerSprite = nullptr;


	BaseTower(float posX = 0.0f , float posY = 0.0f);
	virtual ~BaseTower();
	virtual void Update(float deltaTime);
	virtual void Draw();
	virtual void LevelUp();

protected:
	float p_positionX = 0.0f;
	float p_positionY = 0.0f;
	float p_experiencePoints = 0.0f;
	float p_levelUpThreshold = 0.0f;
	int p_currentLevel = 1;
	int p_maxLevel = 3;
	bool p_isMaxLevel = false;
	float p_towerScale = 0.5f;
	
	virtual void IncrementXPBy(int exp);
	virtual void Init();
	virtual void Attack();
	virtual void SetPosition(float x , float y);

};

