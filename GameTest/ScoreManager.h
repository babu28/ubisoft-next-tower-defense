#pragma once
class ScoreManager
{
public:
	//-------------------------------------------------------------------------------------------------------------------
	//  time is in ms
	//-------------------------------------------------------------------------------------------------------------------
	float maxGameTime = 120000.0f;

	inline static ScoreManager& Instance()
	{
		static ScoreManager instance;
		return instance;
	}
	void Init();
	void Update(float deltaTime);
	void Render();
	void IncrementScoreBy(int score);
	int GetScore();
	bool IsGameOver();

private:
	int m_score = 0;
	float m_gametimer = 0.0f;
	bool m_isGameOver = false;

	inline explicit ScoreManager()
	{
	}

	inline ~ScoreManager()
	{
	}

	inline explicit ScoreManager(ScoreManager const&)
	{
	}

	inline ScoreManager& operator=(ScoreManager const&)
	{
		return *this;
	}
};
